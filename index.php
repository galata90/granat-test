<?php

function sum($a, $b)
{
    $max = strlen($a);
    if (strlen($a) < strlen($b)) {
        $max = strlen($b);
        $a   = str_repeat('0', $max - strlen($a)) . $a;
    } elseif (strlen($a) > strlen($b)) {
        $b = str_repeat('0', $max - strlen($b)) . $b;
    }

    $temp = 0;
    for ($i = $max - 1; $i >= 0; $i--) {
        $temp += (int)$a[$i] + (int)$b[$i];
        $a[$i] = (string)($temp % 10);
        $temp  = (int)($temp / 10);
    }
    if ($temp > 0) {
        $a = (string)$temp . $a;
    }

    return $a;
}


echo "First number: \n";
$firstNum = readline();

echo "Second number: \n";
$secondNum = readline();

echo "First number + Second number = " . sum($firstNum, $secondNum) . "\n";